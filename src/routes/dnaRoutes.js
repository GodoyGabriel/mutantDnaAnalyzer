let express = require('express');
const router = express.Router();
const DnaController = require("../controllers/DnaController");


router.get('/stats', DnaController.dnaResults);
router.post('/mutation', DnaController.isMutant);

module.exports = router;